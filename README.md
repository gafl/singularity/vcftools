# vcftools Singularity container
### Bionformatics package vcftools<br>
A set of tools written in Perl and C++ for working with VCF files. This package only contains the C++ libraries whereas the package perl-vcftools-vcf contains the perl libraries<br>
vcftools Version: 0.1.16<br>
[https://vcftools.github.io/]

Singularity container based on the recipe: Singularity.vcftools_v0.1.16

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build vcftools_v0.1.16.sif Singularity.vcftools_v0.1.16`

### Get image help
`singularity run-help ./vcftools_v0.1.16.sif`

#### Default runscript: STAR
#### Usage:
  `vcftools_v0.1.16.sif --help`<br>
    or:<br>
  `singularity exec vcftools_v0.1.16.sif vcftools --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull vcftools_v0.1.16.sif oras://registry.forgemia.inra.fr/gafl/singularity/vcftools/vcftools:latest`


